<?php
/**
 * User: Dipak Malla
 * Date: 2/12/13
 * Time: 9:19 PM
 * To change this template use File | Settings | File Templates.
 */
namespace libs\db;
use libs\db\Db;
class Records extends Db
{
    private $table = '';
    private $where = array();
    private $values = array();
    private $column = array("*"=>"*");
    private $query_type = 0;
    private $query_data = array();
    private $t_start = false;
    public function setTable($table)
    {
        $this->table = $table;
        return $this;
    }
    public function setWhere($where)
    {
        $this->where = $where;
        return $this;
    }
    public function setValues($values)
    {
        $this->values = $values;
        return $this;
    }
    public function setColumn($column)
    {
        $this->column = $column;
        return $this;
    }
    public function __construct($dsn='mysql:host=localhost;dbname=sush', $username='root', $password='', $driver_options=array())
    {
        try
        {
            parent::__construct($dsn,$username,$password,$driver_options);
        }
        catch(\Exception $ex)
        {
            throw new \Exception($ex->getMessage(),$ex->getCode());
        }

    }
    public function Insert()
    {
        try
        {
            if(!$this->t_start)
                if(!$this->getConnection()->beginTransaction()) throw new \Exception("Could not start transaction",908);
            else
                $this->t_start = true;
            $this->InsertQ();
            return $this;
        }
        catch(\Exception $ex)
        {
            throw new \Exception($ex->getMessage(),$ex->getCode());
        }
    }
    public function Select()
    {
        $this->SelectQ();
        return $this->Query();
    }
    public function Update()
    {
        if(!$this->t_start)
            if(!$this->getConnection()->beginTransaction()) throw new \Exception("Could not start transaction",908);
            else
                $this->t_start = true;
        $this->UpdateQ();
        return $this->Query();
    }
    public function Delete()
    {
        if(!$this->t_start)
            if(!$this->getConnection()->beginTransaction()) throw new \Exception("Could not start transaction",908);
            else
                $this->t_start = true;
        $this->DeleteQ();
        return $this;
    }
    public function Save()
    {
        $this->getConnection()->commit();
    }
    public function Cancel()
    {
        $this->getConnection()->rollBack();
    }
    private function DeleteQ()
    {
        try
        {
            //Make Delete Query.
            $sql = '';
            if(!empty($this->table))
            {
                $sql = "DELETE FROM ".$this->table;
                //Finally Check For Where Clause
                $col = array();
                $val = array();
                foreach($this->where as $k=>$v)
                {
                    $col[] = $k."=?";
                    $val[] = $v;
                }
                //Ok Now Final SQL Query ;)
                //TODO Only for AND also need implement for OR.
		unset($this->query_data);
                $sql .= (count($col) > 0) ? " WHERE ".join(" AND ",$col) : "";
                $this->query_data[] = $sql;
                $this->query_data[] = $val;

            }
            else
            {
                throw new \Exception("No table given.",88);
            }
        }
        catch(\Exception $ex)
        {
            throw new \Exception($ex->getMessage(),$ex->getCode());
        }
    }
    private function UpdateQ()
    {
        try
        {
            //Make Update Query.
            $sql = '';
            if(!empty($this->table))
            {
                $sql = "UPDATE ".$this->table." SET ";
                //Finally Check For Columns.
                $col = array();
                $val = array();
                foreach($this->values as $k=>$v)
                {
                    $col[] = $k." = ?";
                    $val[] = $v;
                }
                //Ok Now Final SQL Query ;)
                $sql .= (count($col) > 0) ? "   ".join(",",$col) : "";
                $w = array();
                foreach($this->where as $k=>$v)
                {
                    $w[] = $k."=?";
                    $val[] = $v;
                }
                $sql .= (count($col) > 0) ? " WHERE ".join(" AND ",$w) : "";
                unset($this->query_data);
                $this->query_data[] = $sql;
                $this->query_data[] = $val;

            }
            else
            {
                throw new \Exception("No table given.",88);
            }
        }
        catch(\Exception $ex)
        {
            throw new \Exception($ex->getMessage(),$ex->getCode());
        }
    }
    private function InsertQ()
    {
        try
        {
            //Make Insert Query.
            $sql = '';
            if(!empty($this->table))
            {
                if(count($this->column) >= 1 && array_key_exists("*",$this->column))
                {
                    $sql = "INSERT INTO ".$this->table;
                }
                else
                {
                    $sql = "INSERT INTO ".$this->table." (".join(",",$this->column).")";
                }
                //Finally Check For Columns.
                $col = array();
                $val = array();
                foreach($this->values as $k=>$v)
                {
                    $col[] = "?";
                    $val[] = $v;
                }
                //Ok Now Final SQL Query ;)
		unset($this->query_data);
                $sql .= (count($col) > 0) ? " VALUES( ".join(",",$col).")" : "";
                $this->query_data[] = $sql;
                $this->query_data[] = $val;

            }
            else
            {
                throw new \Exception("No table given.",88);
            }
        }
        catch(\Exception $ex)
        {
            throw new \Exception($ex->getMessage(),$ex->getCode());
        }
    }
    private function SelectQ()
    {
        try
        {
            //Make Select Query.
            $sql = '';
            if(!empty($this->table))
            {
                if(count($this->column) >= 1 && array_key_exists("*",$this->column))
                {
                   $sql = "SELECT  * FROM ".$this->table;
                }
                else
                {
                    $sql = "SELECT ".join(",",$this->column)." FROM ".$this->table;
                }
                //Finally Check For Where Clause
                $col = array();
                $val = array();
                foreach($this->where as $k=>$v)
                {
                    $col[] = $k."=?";
                    $val[] = $v;
                }
                //Ok Now Final SQL Query ;)
                //TODO Only for AND also need implement for OR.
                $sql .= (count($col) > 0) ? " WHERE ".join(" AND ",$col) : "";
		unset($this->query_data);
                $this->query_data[] = $sql;
                $this->query_data[] = $val;

            }
            else
            {
                throw new \Exception("No table given.",88);
            }
        }
        catch(\Exception $ex)
        {
            throw new \Exception($ex->getMessage(),$ex->getCode());
        }
    }
    public function Query()
    {
        try
        {
            $a = $this->query_data;
            $stm = $this->getConnection()->prepare($a[0]);
            $res = $stm->execute($a[1]);
            if($res)
                return $stm;
            else
                throw new \Exception(join("||",$this->getConnection()->errorInfo()),89);
        }
        catch(\Exception $ex)
        {
            throw new \Exception($ex->getMessage(),$ex->getCode());
        }

    }
}
