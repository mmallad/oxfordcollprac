<?php
/**
 * User: Dpak Malla
 * Date: 2/12/13
 * Time: 9:07 PM
 */
namespace libs\db;
class Db
{
    private $pdo = null;
    public function __construct($dsn, $username='', $password='', $driver_options=array())
    {

        try
        {
            //Open Database Channel with PDO
            //Create PDO object.
            $this->pdo = new \PDO($dsn,$username,$password,$driver_options);
        }
        catch(\PDOException $ex)
        {
            throw new \Exception($ex->getMessage(),$ex->getCode());
        }

    }
    public function getConnection()
    {
        return $this->pdo;
    }
}
